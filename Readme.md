# isit-a-cat-predict-go

This service is the prediction part of the isit-a-cat application.

## Sequences
Service startup:
- load exported tensorflow model from object storage
- import and create tensorflow graph

On message:
- unmarshal message
- fetch image with given id from object storage
- create tensor from input image
- run prediction in tensorflow session
- create response message, marshal
- send on replyTo subject of received message
 
## Libraries
- nats messaging
- tensorflow
- msgpack
- testify 
- prometheus

## Configuration

The following environment variables are relevant for the application:

| Variable Name                             | Mandatory | Default value                  | Description                                                        |
|-------------------------------------------|-----------|--------------------------------|--------------------------------------------------------------------|
| GCLOUD_CREDENTIALS_JSON                   | yes       | -                              | The fully qualified path to the google cloud credentials json file |
| SERVICE_HOST_PORT                         | no        | 0.0.0.0:8080                   | The host and port the service should bind to                       |
| STORAGE_BUCKET_NAME                       | no        | isit-a-cat                     | The google cloud storage bucket name to use                        |
| STORAGE_UPLOADED_IMAGES_FOLDER            | no        | uploaded-images/               | The google cloud storage folder for uploaded images                |
| STORAGE_EXPORTED_MODEL_FOLDER             | no        | keras-exported-model/          | The google cloud storage folder for the exported model             |
| STORAGE_EXPORTED_MODEL_MODEL_FILE_NAME    | no        | model.pb                       | The file name of the exported model                                |
| STORAGE_EXPORTED_MODEL_LABELS_FILE_NAME   | no        | labels.csv                     | The file name of the labels file                                   |
| MESSAGING_SUBJECT_NAME                    | no        | predictions                    | The NATS topic name for predictions                                |
| MESSAGING_BROKER_URL                      | no        | 0.0.0.0:4222                   | The url of the NATS broker                                         |
| MESSAGING_QUEUE_GROUP_NAME                | no        | predictionEventSubscribers     | the name of the nats queue group for receiving predictions         |
| INPUT_PICTURE_COLOR_CHANNELS              | no        | 3                              | The color channels of the input image. Defaults to colour image    |