package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

// Service contains all the relevant application prometheus metrics
type Service struct {
	requestsTotal      prometheus.Counter
	requestsDuration   prometheus.Histogram
	storageTotal       *prometheus.CounterVec
	storageDuration    *prometheus.HistogramVec
	predictionTotal    prometheus.Counter
	predictionDuration prometheus.Histogram
}

// New creates a new instance of Service
func New() *Service {
	return &Service{
		requestsTotal: promauto.NewCounter(prometheus.CounterOpts{
			Name: "requests_total",
			Help: "The total number of incoming requests",
		}),
		requestsDuration: promauto.NewHistogram(prometheus.HistogramOpts{
			Name: "request_duration_seconds",
			Help: "The request duration in seconds",
		}),
		storageTotal: promauto.NewCounterVec(prometheus.CounterOpts{
			Name: "storage_total",
			Help: "The total number of calls to the cloud imageupload",
		}, []string{"objectId"}),
		storageDuration: promauto.NewHistogramVec(prometheus.HistogramOpts{
			Name: "storage_duration_seconds",
			Help: "The duration of accessing cloud imageupload in seconds",
		}, []string{"objectId"}),
		predictionTotal: promauto.NewCounter(prometheus.CounterOpts{
			Name: "prediction_total",
			Help: "The total number of predicted images",
		}),
		predictionDuration: promauto.NewHistogram(prometheus.HistogramOpts{
			Name: "prediction_duration_seconds",
			Help: "The duration of image prediction in seconds",
		}),
	}
}

// RegisterIncomingRequest and observe duration
func (s *Service) RegisterIncomingRequest(duration float64) {
	s.requestsTotal.Inc()
	s.requestsDuration.Observe(duration)
}

// RegisterPrediction and observe duration
func (s *Service) RegisterPrediction(duration float64) {
	s.predictionTotal.Inc()
	s.predictionDuration.Observe(duration)
}

//RegisterStorage operation and observe duration
func (s *Service) RegisterStorage(duration float64, objectId string) {
	s.storageTotal.WithLabelValues(objectId).Inc()
	s.storageDuration.WithLabelValues(objectId).Observe(duration)
}
