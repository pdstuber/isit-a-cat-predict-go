module gitlab.com/pdstuber/isit-a-cat-predict-go

go 1.13

require (
	github.com/gocarina/gocsv v0.0.0-20200330101823-46266ca37bd3
	github.com/gorilla/mux v1.7.4
	github.com/minio/minio-go/v6 v6.0.57
	github.com/nats-io/jwt v1.0.1 // indirect
	github.com/nats-io/nats-server/v2 v2.1.0 // indirect
	github.com/nats-io/nats.go v1.10.0
	github.com/nats-io/nkeys v0.2.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.6.0
	github.com/prometheus/common v0.10.0 // indirect
	github.com/prometheus/procfs v0.1.1 // indirect
	github.com/stretchr/testify v1.5.1
	github.com/tensorflow/tensorflow v2.1.0+incompatible
	github.com/vmihailenco/msgpack/v4 v4.3.11
	github.com/vmihailenco/msgpack/v5 v5.0.0-beta.1
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9 // indirect
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
	golang.org/x/sys v0.0.0-20200610111108-226ff32320da // indirect
	google.golang.org/protobuf v1.24.0 // indirect
)
