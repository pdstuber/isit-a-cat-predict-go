package storage

import (
	"github.com/minio/minio-go/v6"
	"github.com/pkg/errors"
	"io/ioutil"
	"log"
)

const (
	errorTextCouldNotCreateClient = "could not create storage client"
	errorTextBucketRead           = "could not read from bucket"
)

// Service handles writes and reads from object storage buckets
type Service struct {
	storageBucketName string
	objectReader      ObjectReader
}

// A ObjectReaderWriter reads and writes storage bucket objects
type ObjectReader interface {
	GetObject(bucketName, objectName string, opts minio.GetObjectOptions) (*minio.Object, error)
}

// New creates a new storage service instance
func New(storageBucketName string, endpoint string, accessKeyID string, secretAccessKey string, secure bool) (*Service, error) {
	minioClient, err := minio.New(endpoint, accessKeyID, secretAccessKey, secure)

	if err != nil {
		return nil, errors.Wrap(err, errorTextCouldNotCreateClient)
	}

	return &Service{
		storageBucketName: storageBucketName,
		objectReader:      minioClient,
	}, nil
}

// ReadFromBucketObject reads data from the bucket object with the given ID and folder name
func (service *Service) ReadFromBucketObject(objectFolder string, objectId string) ([]byte, error) {
	storageObjectPath := objectFolder + objectId

	log.Printf("Trying to read from %v/%v\n", service.storageBucketName, storageObjectPath)

	object, err := service.objectReader.GetObject(service.storageBucketName, storageObjectPath, minio.GetObjectOptions{})

	if err != nil {
		return nil, errors.Wrap(err, errorTextBucketRead)
	}

	defer func() {
		err := object.Close()
		if err != nil {
			log.Printf("Error in closing object: %v\n", err)
		}
	}()

	data, err := ioutil.ReadAll(object)

	log.Printf("Successfully read %v bytes from %v/%v\n", len(data), service.storageBucketName, storageObjectPath)

	return data, err
}
