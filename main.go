package main

import (
	"context"
	"github.com/gocarina/gocsv"
	"github.com/gorilla/mux"
	"github.com/nats-io/nats.go"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/pdstuber/isit-a-cat-predict-go/metrics"
	"gitlab.com/pdstuber/isit-a-cat-predict-go/prediction"
	"gitlab.com/pdstuber/isit-a-cat-predict-go/prediction/tensorflow"
	"gitlab.com/pdstuber/isit-a-cat-predict-go/storage"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"strconv"
	"time"
)

var (
	objectStorageEndpoint              string
	objectStorageAccessKeyID           string
	objectStorageSecretAccessKey       string
	objectStorageUseTLS                bool
	hostPort                           string
	objectStorageBucketName            string
	storageUploadedImagesFolder        string
	storageExportedModelFolder         string
	storageExportedModelModelName      string
	storageExportedModelLabelsName     string
	predictionsMessagingSubjectName    string
	messagingBrokerUrl                 string
	predictionsMessagingQueueGroupName string
	colorChannels                      int

	labels []tensorflow.Label

	predictionService *tensorflow.Service

	natsConnection *nats.Conn

	predictionHandler *prediction.Handler
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func init() {
	initEnvConfig()

	metricsService := metrics.New()

	var err error
	storageService, err := storage.New(objectStorageBucketName, objectStorageEndpoint, objectStorageAccessKeyID, objectStorageSecretAccessKey, objectStorageUseTLS)

	if err != nil {
		log.Fatalf("could not create storage service: %v\n", err)
	}

	start := time.Now()

	model, err := storageService.ReadFromBucketObject(storageExportedModelFolder, storageExportedModelModelName)

	elapsed := time.Since(start)
	go metricsService.RegisterStorage(elapsed.Seconds(), storageExportedModelModelName)

	if err != nil {
		log.Fatalf("could not get model from cloud storage: %v\n", err)
	}

	start = time.Now()

	labelBytes, err := storageService.ReadFromBucketObject(storageExportedModelFolder, storageExportedModelLabelsName)

	elapsed = time.Since(start)
	go metricsService.RegisterStorage(elapsed.Seconds(), storageExportedModelLabelsName)

	if err != nil {
		log.Fatalf("could not get labels from cloud storage: %v\n", err)
	}

	if err := gocsv.UnmarshalBytes(labelBytes, &labels); err != nil {
		log.Fatalf("could not unmarshal labels csv: %v\n", err)
	}

	natsConnection, err = nats.Connect(messagingBrokerUrl)

	predictionService = tensorflow.New(model, labels)

	if err != nil {
		log.Fatalf("Error in connecting to NATS: %v\n", err)
	}

	predictionHandler = prediction.NewHandler(metricsService, predictionService, storageService, int64(colorChannels), storageUploadedImagesFolder, natsConnection)
}

func main() {
	server := createHTTPServer()

	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Fatalf("Failed to bring up metrics server. Shutting down! %v", err)
		}
	}()

	_, err := natsConnection.QueueSubscribe(predictionsMessagingSubjectName, predictionsMessagingQueueGroupName, handleMessage)

	if err != nil {
		log.Fatalf("Error in setting up the subscription on subject %v: %v/n", predictionsMessagingSubjectName, err)
	}

	blockAndWaitForSignal(natsConnection, predictionService, server)
}

func handleMessage(msg *nats.Msg) {
	log.Printf("Received a message on '%v %v': %v", msg.Subject, msg.Reply, msg.Data)
	go predictionHandler.CalculateAndPublishPrediction(msg.Data, msg.Reply)
}

func initEnvConfig() {
	objectStorageEndpoint = getEnv("OBJECT_STORAGE_ENDPOINT", "")
	objectStorageAccessKeyID = getEnv("MINIO_ACCESS_KEY", "")
	objectStorageSecretAccessKey = getEnv("MINIO_SECRET_KEY", "")
	objectStorageUseTLS, _ = strconv.ParseBool(getEnv("OBJECT_STORAGE_USE_TLS", "false"))
	hostPort = getEnv("SERVICE_HOST_PORT", "0.0.0.0:8080")
	objectStorageBucketName = getEnv("STORAGE_BUCKET_NAME", "isit-a-cat")
	storageUploadedImagesFolder = getEnv("STORAGE_UPLOADED_IMAGES_FOLDER", "uploaded-images/")
	storageExportedModelFolder = getEnv("STORAGE_EXPORTED_MODEL_FOLDER", "keras-exported-model/")
	storageExportedModelModelName = getEnv("STORAGE_EXPORTED_MODEL_MODEL_FILE_NAME", "model.pb")
	storageExportedModelLabelsName = getEnv("STORAGE_EXPORTED_MODEL_LABELS_FILE_NAME", "labels.csv")
	predictionsMessagingSubjectName = getEnv("PREDICTIONS_MESSAGING_SUBJECT_NAME", "predictions")
	messagingBrokerUrl = getEnv("MESSAGING_BROKER_URL", "0.0.0.0:4222")
	predictionsMessagingQueueGroupName = getEnv("MESSAGING_QUEUE_GROUP_NAME", "predictionEventSubscribers")
	colorChannels, _ = strconv.Atoi(getEnv("INPUT_PICTURE_COLOR_CHANNELS", "3"))
}

func blockAndWaitForSignal(conn *nats.Conn, predictionService *tensorflow.Service, server *http.Server) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	<-c
	ctx, cancel := context.WithTimeout(context.Background(), 2)
	defer cancel()
	log.Println("shutting down...")
	predictionService.Close()

	//TODO log errors like in isit-a-cat-bff
	_ = server.Shutdown(ctx)
	_ = conn.Drain()
	os.Exit(0)
}

func handlePing() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}
}

func createHTTPServer() *http.Server {
	router := mux.NewRouter()
	router.Handle("/metrics", promhttp.Handler())
	router.HandleFunc("/ping", handlePing())

	return &http.Server{
		Addr:         hostPort,
		WriteTimeout: time.Millisecond * 300,
		ReadTimeout:  time.Millisecond * 300,
		IdleTimeout:  time.Millisecond * 300,
		Handler:      router,
	}
}
