package prediction

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/vmihailenco/msgpack/v4"
	"gitlab.com/pdstuber/isit-a-cat-predict-go/prediction/mocks"
	"gitlab.com/pdstuber/isit-a-cat-predict-go/prediction/model"
	"strings"
	"testing"
)

const (
	testImageColorChannels  = int64(3)
	testStorageUploadFolder = "test"
	testImageID             = "123"
	testReplyToSubject      = "reply"
	mockErrorText           = "everything went to hell"
)

var (
	testPredictionInput  = model.Input{ID: testImageID}
	testMessage, _       = msgpack.Marshal(&testPredictionInput)
	invalidTestMessage   = []byte{9, 8, 7, 6, 5}
	mockPredictionResult = model.Result{
		Class:       "banana",
		Probability: 0.99,
	}
	expectedErrorResult               = model.ErrorResult{Message: predictionErrorMessage}
	mockImage                         = []byte{1, 2, 3, 4, 5, 6, 7}
	metricsPublisherMock              *mocks.MetricsPublisher
	mockPredictionResultMarshalled, _ = msgpack.Marshal(&mockPredictionResult)
	expectedErrorResultMarshalled, _  = msgpack.Marshal(&expectedErrorResult)
	mockError                         = errors.New(mockErrorText)
)

func init() {
	metricsPublisherMock = new(mocks.MetricsPublisher)

	metricsPublisherMock.On("RegisterIncomingRequest", mock.Anything).Return()
	metricsPublisherMock.On("RegisterPrediction", mock.Anything).Return()
	metricsPublisherMock.On("RegisterStorage", mock.Anything, mock.Anything).Return()

}
func Test_CalculateAndPublishPrediction_good_case(t *testing.T) {
	imagePredictorMock := new(mocks.ImagePredictor)
	messagingConnectorMock := new(mocks.MessagingConnector)
	storageReaderMock := new(mocks.StorageReader)

	imagePredictorMock.On("PredictImage", mock.Anything, mock.Anything).Return(&mockPredictionResult, nil)
	messagingConnectorMock.On("Publish", mock.Anything, mock.Anything).Return(nil)
	storageReaderMock.On("ReadFromBucketObject", mock.Anything, mock.Anything).Return(mockImage, nil)

	predictionHandler := NewHandler(metricsPublisherMock, imagePredictorMock, storageReaderMock, testImageColorChannels, testStorageUploadFolder, messagingConnectorMock)

	predictionHandler.CalculateAndPublishPrediction(testMessage, testReplyToSubject)

	imagePredictorMock.AssertCalled(t, "PredictImage", mockImage, testImageColorChannels)
	messagingConnectorMock.AssertCalled(t, "Publish", testReplyToSubject, mockPredictionResultMarshalled)
	storageReaderMock.AssertCalled(t, "ReadFromBucketObject", testStorageUploadFolder, testImageID)
}

func Test_CalculateAndPublishPrediction_error(t *testing.T) {
	imagePredictorMock := new(mocks.ImagePredictor)
	messagingConnectorMock := new(mocks.MessagingConnector)
	storageReaderMock := new(mocks.StorageReader)

	imagePredictorMock.On("PredictImage", mock.Anything, mock.Anything).Return(&mockPredictionResult, nil)
	messagingConnectorMock.On("Publish", mock.Anything, mock.Anything).Return(nil)
	storageReaderMock.On("ReadFromBucketObject", mock.Anything, mock.Anything).Return(mockImage, nil)

	predictionHandler := NewHandler(metricsPublisherMock, imagePredictorMock, storageReaderMock, testImageColorChannels, testStorageUploadFolder, messagingConnectorMock)

	predictionHandler.CalculateAndPublishPrediction(invalidTestMessage, testReplyToSubject)

	imagePredictorMock.AssertNumberOfCalls(t, "PredictImage", 0)
	messagingConnectorMock.AssertCalled(t, "Publish", testReplyToSubject, expectedErrorResultMarshalled)
	storageReaderMock.AssertNumberOfCalls(t, "ReadFromBucketObject", 0)
}

func Test_calculatePrediction_error_unmarshal_input(t *testing.T) {
	imagePredictorMock := new(mocks.ImagePredictor)
	messagingConnectorMock := new(mocks.MessagingConnector)
	storageReaderMock := new(mocks.StorageReader)

	imagePredictorMock.On("PredictImage", mock.Anything, mock.Anything).Return(&mockPredictionResult, nil)
	storageReaderMock.On("ReadFromBucketObject", mock.Anything, mock.Anything).Return(mockImage, nil)

	predictionHandler := NewHandler(metricsPublisherMock, imagePredictorMock, storageReaderMock, testImageColorChannels, testStorageUploadFolder, messagingConnectorMock)

	prediction, err := predictionHandler.calculatePrediction(invalidTestMessage)

	imagePredictorMock.AssertNumberOfCalls(t, "PredictImage", 0)
	storageReaderMock.AssertNumberOfCalls(t, "ReadFromBucketObject", 0)

	assert.Error(t, err)
	assert.Nil(t, prediction)
	assert.Condition(t, func() (success bool) {
		return strings.Contains(err.Error(), errorTextCouldNotUnmarshalIncomingMessage)
	})
}

func Test_calculatePrediction_error_download_image_from_storage(t *testing.T) {
	imagePredictorMock := new(mocks.ImagePredictor)
	messagingConnectorMock := new(mocks.MessagingConnector)
	storageReaderMock := new(mocks.StorageReader)

	imagePredictorMock.On("PredictImage", mock.Anything, mock.Anything).Return(&mockPredictionResult, nil)
	storageReaderMock.On("ReadFromBucketObject", mock.Anything, mock.Anything).Return(nil, mockError)

	predictionHandler := NewHandler(metricsPublisherMock, imagePredictorMock, storageReaderMock, testImageColorChannels, testStorageUploadFolder, messagingConnectorMock)

	prediction, err := predictionHandler.calculatePrediction(testMessage)

	imagePredictorMock.AssertNumberOfCalls(t, "PredictImage", 0)
	storageReaderMock.AssertCalled(t, "ReadFromBucketObject", testStorageUploadFolder, testImageID)

	assert.Error(t, err)
	assert.Nil(t, prediction)
	assert.Condition(t, func() (success bool) {
		return strings.Contains(err.Error(), errorTextCouldNotFetchImageFromStorage)
	})
}

func Test_calculatePredictionCalculateAndPublishPrediction_error_prediction(t *testing.T) {
	imagePredictorMock := new(mocks.ImagePredictor)
	messagingConnectorMock := new(mocks.MessagingConnector)
	storageReaderMock := new(mocks.StorageReader)

	imagePredictorMock.On("PredictImage", mock.Anything, mock.Anything).Return(nil, mockError)
	storageReaderMock.On("ReadFromBucketObject", mock.Anything, mock.Anything).Return(mockImage, nil)

	predictionHandler := NewHandler(metricsPublisherMock, imagePredictorMock, storageReaderMock, testImageColorChannels, testStorageUploadFolder, messagingConnectorMock)

	prediction, err := predictionHandler.calculatePrediction(testMessage)

	imagePredictorMock.AssertCalled(t, "PredictImage", mockImage, testImageColorChannels)
	storageReaderMock.AssertCalled(t, "ReadFromBucketObject", testStorageUploadFolder, testImageID)

	assert.Error(t, err)
	assert.Nil(t, prediction)
	assert.Condition(t, func() (success bool) {
		return strings.Contains(err.Error(), errorTextCouldNotMakePredictionOnImage)
	})
}
