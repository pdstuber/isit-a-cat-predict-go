package prediction

import (
	"github.com/pkg/errors"
	"github.com/vmihailenco/msgpack/v5"
	"gitlab.com/pdstuber/isit-a-cat-predict-go/prediction/model"
	"log"
	"time"
)

const (
	predictionErrorMessage                    = "could not make prediction on image"
	errorTextCouldNotUnmarshalIncomingMessage = "could not unmarshal incoming message"
	errorTextCouldNotFetchImageFromStorage    = "could not fetch image from object storage"
	errorTextCouldNotMakePredictionOnImage    = "could not make prediction on image"
)

// A MetricsPublisher publishes prometheus metrics
type MetricsPublisher interface {
	RegisterIncomingRequest(duration float64)
	RegisterPrediction(duration float64)
	RegisterStorage(duration float64, objectId string)
}

//A ImagePredictor predicts the class of an image
type ImagePredictor interface {
	PredictImage(imageBytes []byte, colorChannels int64) (*model.Result, error)
}

// A StorageReader reads data from the storage object with the given ID from the given folder
type StorageReader interface {
	ReadFromBucketObject(objectFolder string, objectId string) ([]byte, error)
}

// MessagingConnector publishes message to the message broker
type MessagingConnector interface {
	Publish(subj string, data []byte) error
}

// A Handler for incoming prediction requests
type Handler struct {
	metricsPublisher            MetricsPublisher
	imagePredictor              ImagePredictor
	storageReader               StorageReader
	colorChannels               int64
	storageUploadedImagesFolder string
	messagingConnector          MessagingConnector
}

// NewHandler creates a new instance of the Handler
func NewHandler(metricsPublisher MetricsPublisher, imagePredictor ImagePredictor, storageReader StorageReader, colorChannels int64, storageUploadedImagesFolder string, messagingConnector MessagingConnector) *Handler {
	return &Handler{
		metricsPublisher:            metricsPublisher,
		imagePredictor:              imagePredictor,
		storageReader:               storageReader,
		colorChannels:               colorChannels,
		storageUploadedImagesFolder: storageUploadedImagesFolder,
		messagingConnector:          messagingConnector,
	}
}

// CalculateAndPublishPrediction to the given replyToSubject
func (h *Handler) CalculateAndPublishPrediction(incomingMessage []byte, replyToSubject string) {
	start := time.Now()

	defer (func() {
		elapsed := time.Since(start)
		go h.metricsPublisher.RegisterIncomingRequest(elapsed.Seconds())
	})()

	predictionResult, err := h.calculatePrediction(incomingMessage)

	if err != nil {
		log.Printf("could not create prediction response: %v\n", err)
		errorResult := model.ErrorResult{Message: predictionErrorMessage}

		// ignore error. We are creating the error result by hand above
		responseBytes, _ := msgpack.Marshal(&errorResult)
		if err := h.messagingConnector.Publish(replyToSubject, responseBytes); err != nil {
			log.Printf("could not publish error response to subject %s: %v/n", replyToSubject, err)
		}
		return
	}

	// ignore error. We are creating the predicton result by hand above
	responseBytes, _ := msgpack.Marshal(predictionResult)

	if err := h.messagingConnector.Publish(replyToSubject, responseBytes); err != nil {
		log.Printf("Error in publishing response to subject %s: %v/n", replyToSubject, err)
	}
}

func (h *Handler) calculatePrediction(data []byte) (*model.Result, error) {
	input := &model.Input{}

	if err := msgpack.Unmarshal(data, input); err != nil {
		return nil, errors.Wrap(err, errorTextCouldNotUnmarshalIncomingMessage)
	}

	id := input.ID
	start := time.Now()
	image, err := h.storageReader.ReadFromBucketObject(h.storageUploadedImagesFolder, id)
	elapsed := time.Since(start)
	go h.metricsPublisher.RegisterStorage(elapsed.Seconds(), id)

	if err != nil {
		return nil, errors.Wrap(err, errorTextCouldNotFetchImageFromStorage)
	}

	start = time.Now()
	result, err := h.imagePredictor.PredictImage(image, h.colorChannels)
	elapsed = time.Since(start)
	go h.metricsPublisher.RegisterPrediction(elapsed.Seconds())

	if err != nil {
		return nil, errors.Wrap(err, errorTextCouldNotMakePredictionOnImage)
	}

	return result, nil
}
