package model

// the Input for the prediction
type Input struct {
	ID string `json:"id"`
}

// the Result of the prediction
type Result struct {
	Class       string  `json:"class"`
	Probability float32 `json:"probability"`
}

// the ErrorResult of the prediction
type ErrorResult struct {
	Message string `json:"message"`
}
